package br.com.galgo.testes.suite;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.cumpre_eventos_pendentes.CumprirEventosPedentesInformacaoAnbima;
import br.com.galgo.relatorio.GerarRelatorio;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;

@RunWith(Categories.class)
@Suite.SuiteClasses({ CumprirEventosPedentesInformacaoAnbima.class })
public class SuiteEventosPendentesInfoAnbima {

	@BeforeClass
	public static void setUp() throws Exception {
		GerarRelatorio.zerarContadores();
		GerarRelatorio.gerarPrintInicial();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		GerarRelatorio.gerarPrintFinal();
		GerarRelatorio.gerarRelatorio(Servico.INFO_ANBIMA);
	}

}
